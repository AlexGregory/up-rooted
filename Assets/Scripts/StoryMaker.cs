﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryMaker : MonoBehaviour {

	public Text storyText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable() {

		string finalStory = "";

		finalStory += "Unshrouded from winter’s veil,\n" +
			"And through it, a seed prevailed.\n" +
			"Now a sapling, the soil entails, \n" +
			"For spring to tell its tale.\n";
		finalStory += "\n";

		if (GameManager.instance.numBreaks == 0) {
			finalStory += "Through the seasons\n"+
				"The tree grew tall and true,\n" +
				"Thus few would argue\n" +
				"That others would rue\n" +
				"How well tree would do.\n";
		}
		else if (GameManager.instance.numBreaks == 1) {
			finalStory += "Though there are few\n" +
				"Who stand the test of time\n" +
				"One may fall,\n" +
				"But one out of the blue\n" +
				"May best all.\n";
		}
		else if (GameManager.instance.numBreaks < 10) {
			finalStory += "So long as roots stay planted,\n" +
				"Life shall be granted.\n";

		}
		else {
			finalStory += "THERE IS ONLY SO MUCH POETRY\n" +
				" I CAN WRITE ABOUT A TREE!\n" +
				"I STOPPED COUNTING HOW MANY TIMES\n" +
				"SO LET IT FREE,\n" +
				"OR PAY FOR YOUR CRIMES!\n";
		}
		finalStory += "\n";

		if (Mathf.Abs (Helpers.getBranchRightWeight() - Helpers.getBranchLeftWeight()) < 3) {
			finalStory += "Srong and stable\n" +
				"If it were able\n" +
				"This would make a fine table!\n";
			finalStory += "\n";

		} else if (Helpers.getBranchRightWeight() > Helpers.getBranchLeftWeight()) {
			finalStory += "One that leans right\n " +
				"Has great might\n" +
				"Nut such power\n" +
				"Had fell the tower.\n";
			finalStory += "\n";
		}
		else {
			finalStory += "The left is not wrong\n" +
				"Yet not praised in song.\n" +
				"Find the balance\n" +
				"To show off your talents\n";
			finalStory += "\n";
		}
			
		if (Helpers.getBranchRightWeight() < 3) {
			finalStory += "Less than three right?\n" +
				"That isn't that tight.\n" +
				"Now look what's left.\n";
		} else if (Helpers.getBranchRightWeight() > 10) {
			finalStory += "You know your rights!\n";
		} else {
			// finalStory += "Not too right, not too left.";
		}
		finalStory += "\n";

		if (Helpers.getBranchLeftWeight() < 3) {
			finalStory += "Less than three right?\n" +
				"You are all that is left.\n";
		} else if (Helpers.getBranchLeftWeight() > 10) {
			finalStory += "You got many of what is left.\n";
		} else {
			// finalStory += "Not too right, not too left.";
		}
		finalStory += "\n";

		if (Helpers.getRootHeight() > 10) {
			finalStory += "Some believe one must remember\n" +
				"That the deep beginings to timber\n" +
				"Lay below the soot.\n" +
				"The foundation\n" +
				"May birth the nation\n" +
				"Due to one's roots.\n";
		} else if (Helpers.getRootHeight() > 7) {
			finalStory += "Where one has strength,\n" +
				"One can dig near\n" +
				"Till roots reach length\n" +
				"And see what brings a tear.\n";
		} else if (Helpers.getRootHeight() > 5) {
			finalStory += "The average has strong roots,\n" +
				"But the greater reaches higher.\n";
		} else if (Helpers.getRootHeight() > 3) {
			finalStory += "Dig deep,\n" +
				"See the balance,\n" +
				"For it is the tip of the iceburge.\n";
		} else  {
			finalStory += "One may be tall,\n" +
				"But if one has no root,\n" +
				"Then all will fall.\n" +
				"Stability shall follow suit.\n";
		}
		finalStory += "\n";

		finalStory += "Rise tall, or fall short,\n" +
			"If you are far from the floor,\n" +
			"Here is your score!\n";




		finalStory += "\n";
		finalStory += "Your Score:" + (((Helpers.getTotalBranchTiles()-1) * 100) + (Helpers.getTotalRootTiles()-1) * 30) ;

		finalStory += "\n";

		if (Helpers.getTotalBranchTiles() > 30) {
			finalStory += "In Business, in thickness,\n" +
				"In skill, in thrill,\n" +
				"One branches out\n" +
				"Success is what it's about.\n";
		} 
		else if (Helpers.getTotalBranchTiles() > 20) {
			finalStory += "When winter comes again,\n" +
				"There may be an avalanch.\n" +
				"Just look at those branches.\n";
		}
		else if (Helpers.getTotalBranchTiles() > 10) {
				finalStory += "One branch grew a tomato," +
					"Once sliced, it asked," +
					"\"Where is he ranch?\"";

		} 
		else if (Helpers.getTotalBranchTiles() > 5) {
			finalStory += "It is like an RPG,\n" +
				"You have a skill tree.\n" +
				"It is good to branch out.\n";
		} 
		else  {
			finalStory += "Just... on a whim,\n" +
				"Grow a limb.\n";
		}
		 

		storyText.text = finalStory;
	}
}
