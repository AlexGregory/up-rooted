﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMaker : MonoBehaviour {

	public GameObject[] clouds;
	public GameObject[] cloudSpawners;
	public float cloudSpawnMaxTimeRange;
	public float cloudSpawnMinTimeRange;
	public float cloudSpeedMinRange;
	public float cloudSpeedMaxRange;
	public float timer;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (timer <= 0) 
		{
			timer = Random.Range (cloudSpawnMaxTimeRange, cloudSpawnMinTimeRange);
			GameObject spawn = cloudSpawners [Random.Range (0, cloudSpawners.Length)];
			GameObject temp = Instantiate (clouds [Random.Range (0, clouds.Length)], spawn.transform.position, Quaternion.identity);
			temp.transform.parent = transform;
			temp.GetComponent<CloudMover> ().speed = Random.Range (cloudSpeedMinRange, cloudSpeedMaxRange);

			if (temp.transform.position.x > 55) 
			{
				temp.GetComponent<CloudMover> ().movementDirection = CloudMover.MovementDirection.rightToLeft;
			}

			if (temp.transform.position.x < 55) 
			{
				temp.GetComponent<CloudMover> ().movementDirection = CloudMover.MovementDirection.leftToRight;
			}

		}

		timer -= Time.deltaTime;
	}
}
