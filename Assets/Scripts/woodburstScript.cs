﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class woodburstScript : MonoBehaviour 
{
	public GameObject particles;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.GetComponent<dirtScript> () != null) 
		{
			GameObject temp = Instantiate (particles, this.GetComponent<Transform> ().position, Quaternion.identity);
			AudioManager.instance.playSound ("branchBreak");
			Destroy (temp, 2.0f);
			Destroy (this.gameObject);
		}	
	}
}
