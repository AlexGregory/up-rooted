﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager instance;

	[Header("Locations and Elements")]
	public RectTransform pieceHome;
	public Text timer;
	// Use this for initialization

	void Awake () {
		// Create the singleton
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}


	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (timer != null) {			
			if (GameManager.instance.timerRemaining % 60 < 10) {
				timer.text = Mathf.Floor (GameManager.instance.timerRemaining / 60) + ":0" + Mathf.Floor (GameManager.instance.timerRemaining % 60);
			} else {
				timer.text = Mathf.Floor (GameManager.instance.timerRemaining / 60) + ":" + Mathf.Floor (GameManager.instance.timerRemaining % 60);
			}
		}

	}
}
