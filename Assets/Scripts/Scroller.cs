﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Scroller : MonoBehaviour {

	public Vector3 endLocation;
	public float speed;
	public float cutoff = 0.1f;
	public UnityEvent OnReachEnd;
	public UnityEvent startEvent;
	private Transform tf;
	private Vector3 startLocation;


	// Use this for initialization
	void Awake () {
		tf = GetComponent<Transform> ();
		startLocation = tf.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (tf.position, endLocation) > cutoff) 
		{
			tf.position = Vector3.MoveTowards (tf.position, endLocation, speed * Time.deltaTime);
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			OnReachEnd.Invoke();
		}

		if (Vector3.Distance (tf.position, endLocation) < cutoff) {
			startEvent.Invoke ();
		}
	}

	void OnEnable () {
		tf.position = startLocation;
		GameManager.instance.cameraObject.transform.position = new Vector3 (24, 15, -10);
	}
	public void startDelayedDisable()
	{
		StartCoroutine (delayedDisable ());
		Debug.Log("Started the Coroutine");
	}
	public IEnumerator delayedDisable()
	{
		yield return new WaitForSeconds (3.0f);
		OnReachEnd.Invoke ();
	}
}
