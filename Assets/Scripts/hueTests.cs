﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hueTests : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown(KeyCode.P)) {
			GameManager.instance.worldDrawer.MakeWorld ();
			GameManager.instance.worldDrawer.DrawWorld ();
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			GameManager.instance.SpawnNewPiece ();
		}


		if (Input.GetKeyDown (KeyCode.Q)) {
			GameManager.instance.toppleTree ();

		}

		if (Input.GetKeyDown (KeyCode.W)) {
			Debug.Log("BranchHeight: " + Helpers.getBranchHeight());
			Debug.Log("BranchLeftWeight: " + Helpers.getBranchLeftWeight());
			Debug.Log("BranchRightWeight: " + Helpers.getBranchRightWeight());
			Debug.Log("BranchTotal: " + Helpers.getTotalBranchTiles());
			Debug.Log("BranchWidth: " + Helpers.getBranchWidth());

			Debug.Log("RootHeight: " + Helpers.getRootHeight());
			Debug.Log("RootLeftWeight: " + Helpers.getRootLeftWeight());
			Debug.Log("RootRightWeight: " + Helpers.getRootRightWeight());
			Debug.Log("RootTotal: " + Helpers.getTotalRootTiles());
			Debug.Log("RootWidth: " + Helpers.getRootWidth());
		}

	}
}
