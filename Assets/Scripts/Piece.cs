﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

	public Transform tf;
	public SpriteRenderer[] spriteRenderers;
	private float distanceFromCamera;
	public Vector3 offset = new Vector3(0,1,0);

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();		
		spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
		distanceFromCamera = Camera.main.transform.position.z - tf.position.z;

		// Go Home!
		GameManager.instance.GoHomePiece(this);
	}
	
	// Update is called once per frame
	void Update () {
		List<Vector2> dropTiles = new List<Vector2>();

		foreach (Transform child in transform) {
			dropTiles.Add (Helpers.WorldToGridPoint (child.position));
		} 

		if (GameManager.instance.IsPlacable (dropTiles)) {
			foreach (SpriteRenderer sr in spriteRenderers) {
				sr.color = Color.green;
			}
		} else {
			foreach (SpriteRenderer sr in spriteRenderers) {
				sr.color = Color.red;
			}
		}
	}

	void OnMouseDown( ) {
	}




	public void OnMouseDrag () {

		if (!GameManager.instance.placementMode) 
		{
			return;
		}

		this.GetComponent<GridSnapper> ().enabled = true;
		// Move to mouse position
		Vector3 newPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition + (Vector3.forward * distanceFromCamera) );
		newPosition.z = tf.position.z;
		newPosition += offset;
		tf.position = newPosition;
	}


	public void OnMouseUp () { 

		Debug.Log ("FINISHED DRAG");

		List<Vector2> dropTiles = new List<Vector2>();
		foreach (Transform child in transform) {
			//Debug.Log(child.name + " dropped at grid location "+childGridx+","+childGridy);
			dropTiles.Add (Helpers.WorldToGridPoint(child.position));
		}

		//TODO: Tell the game manager to attempt to place that child and destroy this object if needed
		GameManager.instance.AttemptToPlacePieces (dropTiles, this);
	}
}
