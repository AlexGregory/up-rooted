﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldDrawer : MonoBehaviour {

	private Transform tf;
	public GameObject[] pf_doodads;
	[Range(0,1)] public float doodadChance = 0.25f;

	public TileType[,] grid;
	public GameObject[,] tiles;
	public GameObject[,] treeTiles;

	private GameObject doodads;

	// Use this for initialization
	void Awake () {
		tf = GetComponent<Transform> ();

		if (doodads == null) {
			doodads = new GameObject ("doodads");
			doodads.transform.parent = tf;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void treeRestart()
	{
		for (int horiz = 0; horiz < GameManager.instance.worldData.width; horiz++) 
		{
			int groundHeight = GameManager.instance.worldData.groundHeight;
			// Start at top and every row down to ground level...
			for (int vert = GameManager.instance.worldData.height - 1; vert >= groundHeight; vert--) {
				// ... is a sky tile.
				if (grid [horiz, vert] == TileType.Branch) 
				{
					grid [horiz, vert] = TileType.Sky;

				}
			}
			CreateTree ();
			grid[GameManager.instance.worldData.treeStartX, groundHeight] = TileType.Branch;
		}
	}
	public void MakeWorld() {

		Debug.Log ("Making World");

		// Reset our doodads
		Destroy(doodads);
		doodads = new GameObject ("doodads");
		doodads.transform.parent = tf;

		// Make our grid
		grid = new TileType[GameManager.instance.worldData.width,GameManager.instance.worldData.height];

		// for our first tile, we are at "start ground height"
		int groundHeight = GameManager.instance.worldData.groundHeight;

		// In case we don't want to start at (0,Y) in world coordinates

		// Start at X of 0, and do every column across
		for (int horiz = 0; horiz < GameManager.instance.worldData.width; horiz++) {

			// Start at top and every row down to ground level...
			for (int vert = GameManager.instance.worldData.height-1; vert >= groundHeight; vert--) {
				// ... is a sky tile.
				grid[horiz,vert] = TileType.Sky;
			}

			// TODO: If up or down, adjust ground level, then also add corners where needed


			// Make ground level 
			grid[horiz, groundHeight] = TileType.Soil;

			// add a doodad, maybe
			if (Random.value < doodadChance && horiz != GameManager.instance.worldData.treeStartX) { 
				GameObject doodad = Instantiate(pf_doodads[Random.Range(0, pf_doodads.Length)], Helpers.GridToWorldPoint(new Vector2(horiz, groundHeight)), Quaternion.identity) as GameObject;
				doodad.transform.parent = doodads.transform;
			}

			// From below ground level down to 0, make dirt and a chance for rocks
			for (int vert = groundHeight-1; vert >= 0; vert--) 
			{
				int roll = Random.Range (1, 101);
				if (roll <= GameManager.instance.worldData.rockPercentageOutOf100) 
				{
					grid [horiz, vert] = TileType.Rock;
				}
				else 
				{
					grid [horiz, vert] = TileType.Dirt;
				}
			}
		}
	
		//Now that the tile array is all sky and dirt and soil, make grid changes for the starting plant.
		grid[GameManager.instance.worldData.treeStartX, groundHeight] = TileType.Branch;
		grid[GameManager.instance.worldData.treeStartX, groundHeight - 1] = TileType.Root;

		//Debug.Log ("World" + grid.GetLength(0) + "," + grid.GetLength(1));

	}

	public void CreateTree () {
		// Delete the old tree
		Destroy(GameManager.instance.tree);

		// Make a new one
		GameManager.instance.tree = new GameObject ("Tree");
		CompositeCollider2D cc = GameManager.instance.tree.AddComponent<CompositeCollider2D> () as CompositeCollider2D;
		GameManager.instance.tree.GetComponent<Rigidbody2D> ().simulated = false;
		GameManager.instance.tree.GetComponent<Rigidbody2D> ().gravityScale = 0;
		GameManager.instance.tree.transform.position = Helpers.GridToWorldPoint (new Vector2 (GameManager.instance.worldData.treeStartX, GameManager.instance.worldData.groundHeight));

	}

	public void DrawWorld () { 

		Debug.Log ("Drawing World");

		// If there is no tile spawned, spawn one
		if (GameManager.instance.currentPiece == null) {
			GameManager.instance.SpawnNewPiece ();
		}

		// Destroy any existing tiles
		if ( tiles != null) {
			foreach (GameObject tile in tiles) {
				Destroy (tile);
			}
		}

		// make the tree
		CreateTree();

		// Create array of tiles
		tiles = new GameObject[GameManager.instance.worldData.width,GameManager.instance.worldData.height];
		treeTiles = new GameObject[GameManager.instance.worldData.width,GameManager.instance.worldData.height];

		// Instantiate the correct tiles
		for (int col = 0; col < GameManager.instance.worldData.width; col++) {
			for (int row = 0; row < GameManager.instance.worldData.height; row++) {

				// Debug.Log ("Drawing Tile:" + col + "," + row );

				// Find position
				Vector3 tilePosition = new Vector3(tf.position.x + col, tf.position.y + row, tf.position.z ); 

				// Based on what tile is in the grid, instantiate that prefab.
				GameObject temp;

				switch (grid[col, row]) {
				case TileType.Sky:
					tiles [col, row] = Instantiate (GameManager.instance.pf_skyTile, tilePosition, Quaternion.identity);
					break;
				case TileType.Soil:
					tiles [col, row] = Instantiate (GameManager.instance.pf_soilTile, tilePosition, Quaternion.identity);
					break;
				case TileType.Dirt:
					tiles [col, row] = Instantiate (GameManager.instance.pf_dirtTile, tilePosition, Quaternion.identity);
					break;
				case TileType.Branch:
					// Create a branch piece and make it part of my tree
					temp = Instantiate (GameManager.instance.pf_treeTile, tilePosition, Quaternion.identity) as GameObject;
					temp.transform.parent = GameManager.instance.tree.transform;
					treeTiles[col,row] = temp;

					// Make a sky and make it part of the tiles
					tiles [col, row] = Instantiate (GameManager.instance.pf_skyTile, tilePosition, Quaternion.identity);
					break;
				case TileType.Root:
					// Make a root and (make it part of my tree ?)
					tiles [col, row] = Instantiate (GameManager.instance.pf_rootTile, tilePosition, Quaternion.identity) as GameObject;
					break;
				case TileType.Rock:
					tiles [col, row] = Instantiate (GameManager.instance.pf_rockTile, tilePosition, Quaternion.identity);
					break;
				}

				// Name the tile
				tiles [col, row].name = "Tile("+col+","+row+")";
				// Make it a child of this object
				tiles[col,row].transform.parent = tf;

			}				
		}

		// Put Leaves behind tiles
		Destroy(GameManager.instance.leafs);
		GameManager.instance.leafs = new GameObject ("Leafs");
		GameManager.instance.leafs.transform.parent = GameManager.instance.tree.transform;

		// Look through the trees
		for (int col = 0; col < treeTiles.GetLength (0); col++) {
			for (int row = 0; row < treeTiles.GetLength (1); row++) {
				// If tile above is sky, put treetop above
				if (grid[col, row] == TileType.Branch && grid[col, row+1] == TileType.Sky) {
					GameObject newBush = Instantiate(GameManager.instance.pf_leavesTile, Helpers.GridToWorldPoint(new Vector2(col,row)), Quaternion.identity);
					newBush.transform.parent = GameManager.instance.leafs.transform;
				}
			}
		}


	}


	public void updateImages()
	{
		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				//Branch Logic
				if (grid [col, row] == TileType.Branch) 
				{
					//Just branch above
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchJustUp;
					}

					//Just branch below
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchJustDown;
						Debug.Log ("just down");
					}

					//Just branch left
					if (isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchJustLeft;
					}

					//Just branch right
					if (isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchJustRight;
					}

					//branch above and below
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch && isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchUpAndDown;
					}

					//branch left and right
					if (isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchLeftAndRight;
					}

					//branch up and right
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchUpAndRight;
					}

					//branch up and left
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchUpAndLeft;
					}

					//branch down and right
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchDownAndRight;
					}

					//branch down and left
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchDownAndLeft;
					}

					//branch all but up
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchAllButUp;
					}

					//branch all but down
					if (isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchAllButDown;
					}

					//branch all but left
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchAllButLeft;
					}

					//branch all but right
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchAllButRight;
					}

					//branch all
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Branch && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Branch && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Branch && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Branch) 
					{
						treeTiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.branchAll;
					}
				}

				//Root Logic
				if (grid [col, row] == TileType.Root) 
				{
					//Just roots above
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsJustUp;
					}

					//Just roots below
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsJustDown;
					}

					//Just roots left
					if (isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsJustLeft;
					}

					//Just roots right
					if (isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsJustRight;
					}

					//roots above and below
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root && isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsUpAndDown;
					}

					//roots left and right
					if (isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsLeftAndRight;
					}

					//roots up and right
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsUpAndRight;
					}

					//roots up and left
					if (isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsUpAndLeft;
					}

					//roots down and right
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsDownAndRight;
					}

					//roots down and left
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsDownAndLeft;
					}

					//roots all but up
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsAllButUp;
					}

					//roots all but down
					if (isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsAllButDown;
					}

					//roots all but left
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsAllButLeft;
					}

					//roots all but right
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsAllButRight;
					}

					//roots all
					if (isValidTileLocation (col, row - 1) && grid [col, row - 1] == TileType.Root && isValidTileLocation (col - 1, row) && grid [col - 1, row] == TileType.Root && isValidTileLocation (col, row + 1) && grid [col, row + 1] == TileType.Root && isValidTileLocation (col + 1, row) && grid [col + 1, row] == TileType.Root) 
					{
						tiles [col, row].GetComponent<SpriteRenderer> ().sprite = GameManager.instance.rootsAll;
					}
				}
			}
		}
	}

	public bool isValidTileLocation(int x, int y)
	{
		if (x >= 0 && x < GameManager.instance.worldData.width && y >= 0 && y < GameManager.instance.worldData.height) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}


	public void OnEnable() {
		GameManager.instance.worldDrawer.MakeWorld ();
		GameManager.instance.worldDrawer.DrawWorld ();
		GameManager.instance.score = 0;
		GameManager.instance.numBreaks = 0;
	}

}





[System.Serializable]
public class WorldData {

	public int width;
	public int height;
	public int groundHeight;
	[Range(0,1)]public float randomUpChance;
	[Range(0,1)]public float randomDownChance;
	public float rockPercentageOutOf100;
	public int treeStartX;

}