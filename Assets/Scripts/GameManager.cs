﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	[Header("World")]
	public WorldDrawer worldDrawer;
	public WorldData worldData;
	public GameObject tree;
	public GameObject leafs;
	public GameObject endScene;
	[Header("Score and PlayerData")]
	public int score;
	public int numBreaks;
	public int timerLength;
	public float timerRemaining;
	public bool placementMode = true;
	public float pauseTimer = 1;
	public float pauseTimerRemaining;
	public GameObject cameraObject;
	public GameObject tutorialText;
	public bool hasShownTutorial = false;
	[Header("Tile Prefabs")]
	public GameObject pf_skyTile;
	public GameObject pf_soilTile;
	public GameObject pf_dirtTile;
	public GameObject pf_treeTile;
	public GameObject pf_rootTile;
	public GameObject pf_rockTile;
	public GameObject pf_leavesTile;
	[Header("Sprites")]
	public Sprite branchJustUp;
	public Sprite branchJustDown;
	public Sprite branchJustLeft;
	public Sprite branchJustRight;
	public Sprite branchUpAndDown;
	public Sprite branchLeftAndRight;
	public Sprite branchUpAndRight;
	public Sprite branchUpAndLeft;
	public Sprite branchDownAndRight;
	public Sprite branchDownAndLeft;
	public Sprite branchAllButUp;
	public Sprite branchAllButDown;
	public Sprite branchAllButLeft;
	public Sprite branchAllButRight;
	public Sprite branchAll;
	public Sprite rootsJustUp;
	public Sprite rootsJustDown;
	public Sprite rootsJustLeft;
	public Sprite rootsJustRight;
	public Sprite rootsUpAndDown;
	public Sprite rootsLeftAndRight;
	public Sprite rootsUpAndRight;
	public Sprite rootsUpAndLeft;
	public Sprite rootsDownAndRight;
	public Sprite rootsDownAndLeft;
	public Sprite rootsAllButUp;
	public Sprite rootsAllButDown;
	public Sprite rootsAllButLeft;
	public Sprite rootsAllButRight;
	public Sprite rootsAll;
	[Header("Pieces")]
	public List<GameObject> pf_Pieces;
	public GameObject currentPiece;

	// Awake
	void Awake () {
		// Create the singleton
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		timerRemaining = timerLength;

		if (tree == null) {
			tree = new GameObject();
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (pauseTimerRemaining > 0) 
		{
			pauseTimerRemaining -= Time.deltaTime;
		}

		if (pauseTimerRemaining <= 0 && placementMode == false && Helpers.isTreeGone() == true) 
		{
			placementMode = true;
			worldDrawer.treeRestart ();
		}

		if (pauseTimerRemaining <= 0 && placementMode == false && Helpers.isTreeGone() != true) 
		{
			pauseTimerRemaining = pauseTimer;
		}
		if (hasShownTutorial == false && numBreaks >= 2) 
		{
			tutorialText.SetActive (true);
		}

		if (hasShownTutorial == false && numBreaks >= 4) 
		{
			tutorialText.SetActive (false);
		}
	}
	public void toppleTree()
	{
		AudioManager.instance.playSound ("treeCrack");
		numBreaks++;
		GameManager.instance.tree.GetComponent<CompositeCollider2D> ().GenerateGeometry ();
		GameManager.instance.tree.GetComponent<Rigidbody2D> ().simulated = true;
		GameManager.instance.tree.GetComponent<Rigidbody2D> ().gravityScale = 1.0f;
		GameManager.instance.tree.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (10.0f, 20.0f));
		placementMode = false;
		pauseTimerRemaining = pauseTimer;
	}

	public bool IsPlacable (List<Vector2> positions ) { 
		// compare against the location of position 0
		if (!Helpers.IsInGrid (positions [0])) {
			Debug.Log("Dropped out of range!");
			return false;
		}

		Debug.Log ("Checking for grid space:" + (int)positions [0].x +","+ (int)positions [0].y);
		TileType testType = worldDrawer.grid [(int)positions [0].x, (int)positions [0].y];


		// At least one grid spot needs to be an anchor or we fail
		bool isOneTileAnAnchor = false;

		// If our test tile is NOT sky or dirt, we fail
		if (testType != TileType.Sky && testType != TileType.Dirt) {
			Debug.Log ("Test tile was not sky and not dirt! -- "+worldDrawer.grid [(int)positions [0].x, (int)positions [0].y]);
			return false; 
		}

		for (int i=0; i<positions.Count; i++) {
			// If a new tile is out of range, we fail
			if ( !Helpers.IsInGrid(positions[i]) ) {
				Debug.Log("Dropped out of range!");
				return false;
			}

			// if our new tile doesn't match our test, we fail
			if (worldDrawer.grid [(int)positions [i].x, (int)positions [i].y] != testType) {
				Debug.Log ("tile not match test");
				return false;
			}

			//TODO: If this spot is an anchor, remember that
			if (Helpers.IsAnchor(positions[i]) ) {
				Debug.Log (positions [i] + " IS an anchor");
				isOneTileAnAnchor = true;
			}
		}

		// If nothing was an anchor - quit
		if (!isOneTileAnAnchor) {
			Debug.Log ("No anchor");
			return false;
		}

		return true;
	}



	// Returns true if we can place the pieces
	public bool AttemptToPlacePieces ( List<Vector2> positions, Piece source ) {
		Debug.Log ("Attempting to place");

		if (IsPlacable (positions)) {
			// if we get here, then all tiles are the same (dirt or sky) and at least one is an anchor.

			// Place them
			PlacePieces (positions); 

			// Redraw the world
			worldDrawer.DrawWorld ();
			worldDrawer.updateImages ();
			// placed, so return true!
			Debug.Log ("Placed it!");

		
			//play the sound for placing a piece
			AudioManager.instance.playSound("treeRustle");
			Helpers.willTreeTopple ();
			// Generate next piece
			SpawnNewPiece();
			return true;

		} else {
			Debug.Log ("Failed to place it!");
			//play the sound for whiffing a piece
			AudioManager.instance.playSound("miss");
			GoHomePiece (source);
			return false;
		}
	}

	public bool PlacePieces (List<Vector2> positions) {
		foreach (Vector2 position in positions) { 
			if (worldDrawer.grid [(int)position.x, (int)position.y] == TileType.Sky) {
				worldDrawer.grid [(int)position.x, (int)position.y] = TileType.Branch;
			} else if (worldDrawer.grid [(int)position.x, (int)position.y] == TileType.Dirt) {
				worldDrawer.grid [(int)position.x, (int)position.y] = TileType.Root;
			} else {
				return false;
			}
		}
		return true;
	}

	// TODO: Coroutine to ease to this position
	public void GoHomePiece (Piece source) { 

		Vector3 position = UIManager.instance.pieceHome.position;
		Vector3 homeInWorld = position;	
		Debug.Log (homeInWorld);
		source.transform.position = homeInWorld;


	}

	public void EndTimer () {
		timerRemaining = 0;
	}


	public void SpawnNewPiece () {
		// Destroy the old one
		Destroy (currentPiece);
		// Create a new one
		currentPiece = Instantiate (pf_Pieces[Random.Range(0,pf_Pieces.Count)]) as GameObject;
	}

}
