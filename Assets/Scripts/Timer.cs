﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		GameManager.instance.timerRemaining = GameManager.instance.timerLength;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.instance.timerRemaining > 0) {
			GameManager.instance.timerRemaining -= Time.deltaTime;
		} else {
			Destroy (GameManager.instance.tree);
			Destroy (GameManager.instance.currentPiece);
			// Time is up --- start end story mode
			GameManager.instance.worldDrawer.gameObject.SetActive(false);
			//GameManager.instance
			GameManager.instance.endScene.SetActive(true);
		}
	}
}
