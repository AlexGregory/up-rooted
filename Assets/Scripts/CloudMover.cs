﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour {
	public enum MovementDirection { leftToRight, rightToLeft, test };
	public MovementDirection movementDirection;
	public float speed;
	public Transform tf;
	// Use this for initialization
	void Start () 
	{
		tf = this.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (movementDirection == MovementDirection.leftToRight) 
		{
			tf.Translate(new Vector3(speed/100, 0, 0));
		}

		if (movementDirection == MovementDirection.rightToLeft) 
		{
			tf.Translate(new Vector3(- speed/100, 0,0));
		} 

		if (tf.position.x > 110 || tf.position.x < 0) 
		{
			Destroy (this.gameObject);
		}
	}
}
