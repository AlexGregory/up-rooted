﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraPan : MonoBehaviour {
	public GameObject cameraObject;
	public bool isPaning = false;
	public Vector3 mouseStartLocation;
	public Vector3 cameraStartPosition;
	public Vector3 currentPieceStartPosition;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButtonDown(1))
		{
			isPaning = true;
			mouseStartLocation = Input.mousePosition;
			cameraStartPosition = cameraObject.transform.position;
			currentPieceStartPosition = GameManager.instance.currentPiece.transform.position;
		}	
		if(Input.GetMouseButtonUp(1))
		{
			isPaning = false;
		}

		if (isPaning == true) 
		{
			cameraObject.transform.position = cameraStartPosition - Input.mousePosition / 4 + mouseStartLocation / 4;
			GameManager.instance.currentPiece.transform.position = currentPieceStartPosition - Input.mousePosition / 4 + mouseStartLocation / 4;

		}
	}
}
