﻿//----------------------------
// This should only contain static functions and variables that are used throughout the program. 
//      We will never make a "Helpers" object, just use  Helpers.Whatever();
// Also: our enums and other types go here

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Helpers {

	public static string gameTitle = "UpRooted";

	public static bool IsInGrid(Vector2 position) 
	{
		return IsInGrid ((int)position.x, (int)position.y);
	}

	public static bool IsInGrid( int x, int y) 
	{
		if (x >= 0 && y >= 0 && x < GameManager.instance.worldData.width && y < GameManager.instance.worldData.height) {
			return true;
		}
		return false;
	}

	public static bool IsAnchor (Vector2 position) {
		// if it's not in the grid, then it is not an anchor
		if (!Helpers.IsInGrid (position)) {
			return false;
		}

		// Vector to check
		Vector2 toCheck;

		// if one unit to the right is in the grid, 
		toCheck = position + Vector2.right;
		if (Helpers.IsInGrid (toCheck)) {
			//check if that unit is A branch or root
			if (Helpers.IsBranchOrRoot (toCheck)) {
				return true;
			}
		}

		toCheck = position + Vector2.down;
		if (Helpers.IsInGrid (toCheck)) {
			//check if that unit is A branch or root
			if (Helpers.IsBranchOrRoot (toCheck)) {
				return true;
			}
		}

		toCheck = position + Vector2.left;
		if (Helpers.IsInGrid (toCheck)) {
			//check if that unit is A branch or root
			if (Helpers.IsBranchOrRoot (toCheck)) {
				return true;
			}
		}

		toCheck = position + Vector2.up;
		if (Helpers.IsInGrid (toCheck)) {
			//check if that unit is A branch or root
			if (Helpers.IsBranchOrRoot (toCheck)) {
				return true;
			}
		}

		// If I get here, none of the adjacent tiles are branches or roots
		return false;
	}

	public static bool IsBranchOrRoot (Vector2 toCheck) {
		if (GameManager.instance.worldDrawer.grid [(int)toCheck.x, (int)toCheck.y] == TileType.Root || GameManager.instance.worldDrawer.grid [(int)toCheck.x, (int)toCheck.y] == TileType.Branch) {
			return true;
		}
		return false;
	}

	public static int getTotalBranchTiles()
	{
		int count = 0;
		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Branch)
				{
						count ++;
				}
			}
		}

	return count;
	}

	public static int getTotalRootTiles()
	{
		int count = 0;
		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Root)
				{
					count ++;
				}
			}
		}

		return count;
	}
	public static int getBranchWidth()
	{
		int minTreeX = 10000;
		int maxTreeX = 0;

		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Branch)
				{
					if (col > maxTreeX) 
					{
						maxTreeX = col;
					}

					if (col < minTreeX) 
					{
						minTreeX = col;
					}
				}
			}
		}
		return maxTreeX - minTreeX + 1;
	}

	public static int getRootWidth()
	{
		int minTreeX = 10000;
		int maxTreeX = 0;

		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Root)
				{
					if (col > maxTreeX) 
					{
						maxTreeX = col;
					}

					if (col < minTreeX) 
					{
						minTreeX = col;
					}
				}
			}
		}
		return maxTreeX - minTreeX + 1;
	}

	public static int getBranchHeight()
	{
		int minTreeY = 10000;
		int maxTreeY = 0;

		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Branch)
				{
					if (col > maxTreeY) 
					{
						maxTreeY = row;
					}

					if (col < minTreeY) 
					{
						minTreeY = row;
					}
				}
			}
		}
		return maxTreeY - minTreeY + 1;
	}

	public static int getRootHeight()
	{
		int minTreeY = 10000;
		int maxTreeY = 0;

		for (int col = 0; col < GameManager.instance.worldData.width; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Root)
				{
					if (row > maxTreeY) 
					{
						maxTreeY = row;
					}

					if (row < minTreeY) 
					{
						minTreeY = row;
					}
				}
			}
		}
		Debug.Log (minTreeY);

		Debug.Log (maxTreeY);
		return maxTreeY - minTreeY + 1;
	}
	public static int getBranchLeftWeight()
	{
		int count = 0;
		for (int col = 0; col < GameManager.instance.worldData.treeStartX; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Branch)
				{
					count ++;
				}
			}

		}
		return count;
	}

	public static int getRootLeftWeight()
	{
		int count = 0;
		for (int col = 0; col < GameManager.instance.worldData.treeStartX; col++) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Root)
				{
					count ++;
				}
			}

		}
		return count;
	}

	public static int getBranchRightWeight()
	{
		int count = 0;
		for (int col = GameManager.instance.worldDrawer.grid.GetLength(0)-1; col > GameManager.instance.worldData.treeStartX; col--) 
		{
			for (int row = 0; row < GameManager.instance.worldDrawer.grid.GetLength(1); row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Branch)
				{
					count ++;
				}
			}

		}
		return count;
	}

	public static int getRootRightWeight()
	{
		int count = 0;
		for (int col = GameManager.instance.worldData.width - 1; col > GameManager.instance.worldData.treeStartX; col--) 
		{
			for (int row = 0; row < GameManager.instance.worldData.height; row++) 
			{
				if (GameManager.instance.worldDrawer.grid [col, row] == TileType.Root)
				{
					count ++;
				}
			}

		}
		return count;
	}


	public static Vector3 GridToWorldPoint (Vector2 inPoint) { 
		// position in grid is based on world positon
		Vector3 worldPosition = GameManager.instance.worldDrawer.transform.position;

		float childWorldx = inPoint.x + worldPosition.x;
		float childWorldy = inPoint.y + worldPosition.y;

		return new Vector3 (childWorldx, childWorldy, 0);
	}


	public static Vector2 WorldToGridPoint (Vector3 inPoint) { 
		// position in grid is based on world positon
		Vector3 worldPosition = GameManager.instance.worldDrawer.transform.position;

		int childGridx = (int)(inPoint.x - worldPosition.x);
		int childGridy = (int)(inPoint.y - worldPosition.y);

		return new Vector2 (childGridx, childGridy);
	}

	public static bool isTreeGone()
	{
		foreach (GameObject tile in GameManager.instance.worldDrawer.treeTiles)
		{
			if (tile != null)
			{
				return false;
			}
		}

		return true;
	}


	public static void willTreeTopple()
	{
		//buffer variables for tree toppling
		float totalBuffer = 3.0f;//buffers the amount of roots to branches
		float widthBuffer = 1.5f;//buffers the width of roots to branches
		float hightBuffer = 2.0f;//buffers hight of roots to branches


		if ((float)getTotalRootTiles () * totalBuffer < getTotalBranchTiles ()) 
		{
			GameManager.instance.toppleTree ();
		} 
		else if ((float)getRootWidth () * widthBuffer < getBranchWidth ()) 
		{
			GameManager.instance.toppleTree ();
		} 
		else if ((float)getRootHeight () * hightBuffer < getBranchHeight ()) 
		{
			GameManager.instance.toppleTree ();
		} 

		else 
		{
			
		}

	}

}



public enum TileType { Dirt, Sky, Rock, Soil, Root, Branch, Bird };