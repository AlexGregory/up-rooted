﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSnapper : MonoBehaviour {
	private int gridSize = 1;
	private Transform tf;
	// Use this for initialization
	void Start () 
	{
		tf = this.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (tf.position.x % gridSize != 0 || tf.position.y % gridSize != 0) 
		{
			float xOffset = (tf.position.x % gridSize);
			float yOffset = (tf.position.y % gridSize);
			float x = tf.position.x - xOffset;
			float y = tf.position.y - yOffset;
			int newX = (int)x;
			int newY = (int)y;
			tf.position = new Vector3 (newX, newY, 0);
		}
	}
}
